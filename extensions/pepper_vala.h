#include "pepper.h"

static pepper_alias pepper_vala_alias = { "Vala Compiler", "valac", "--vapidir ",
    "-o", nullptr, nullptr, "--define=%s%s" };

int pepper_vala_add_pkg(char *pkg) {
    pepper_add_compiler_flag("--pkg %s", pkg);
}

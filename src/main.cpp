#include <stdio.h>
#include <string.h>
#include "pepper.h"

#define COMPILER_FLAG "--compiler"
#define CONFIG_FLAG "--config"
#define HELP_FLAG "--help"
#define BUILD_DIR_FLAG "--builddir"

#define HELP_MESSAGE \
"\
--compiler [name]\tset the compiler to be used to compile the configuration file\n\
--config [name]\tspecify the configuration file to be used\n\
--builddir [path]\tspecify the build directory\n\
--help\tdisplay this help\
"

void spicy() {
    printf("Spicy!\n");
}

void display_help() {
    fprintf(stdout, HELP_MESSAGE);
}

int main(int argc, char **argv) {
    // the default configuration file to build
    char *config_name = "pepper_config.cpp";

    int i = 1;
    for(; i < argc; i++) {
        char *flag = argv[i];
        if(!strcmp(flag, COMPILER_FLAG)) {
            int ii = i + 1;
            if(ii >= argc) pepper_add_error("--compiler requires the name or path to a compiler");
            else {
                pepper_set_compiler(argv[ii]);
                i++;
            }
        } else if(!strcmp(flag, CONFIG_FLAG)) {
            int ii = i + 1;
            if(ii >= argc) pepper_add_error("--config requires a file to be specified");
            else {
                config_name = argv[ii];
                i++;
            }
        } else if(!strcmp(flag, HELP_FLAG)) {
            display_help();
            return 0;
        } else if(!strcmp(flag, BUILD_DIR_FLAG)) {
            int ii = i + 1;
            if(ii >= argc) pepper_add_error("%s requires a path to be specified", BUILD_DIR_FLAG);
            else {
                pepper_build_dir(argv[ii]);
                i++;
            }
        }
    }

    // set the source configuration file
    pepper_add_compiler_flag(config_name);
    // set the name of the output file
#if defined(__windows__)
    pepper_add_compiler_flag("/Fepepper_build");
    pepper_add_compiler_flag("/I ./");
    pepper_add_compiler_flag("/I include/");
#elif defined(__linux__)
    pepper_add_compiler_flag("-o pepper_build");
    pepper_add_compiler_flag("-I ./");
    pepper_add_compiler_flag("-I include/");
    pepper_add_compiler_flag("-w");
#endif

    pepper_build(true);
    pepper_output_errors();

    pepper_exec("rm *.obj", NULL, 0, false);
    return 0;
}

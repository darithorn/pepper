#include "pepper.h"

int main() {
#if defined(__windows__)
    pepper_add_compiler_flag("src/main.cpp");
    pepper_add_compiler_flag("/I ./include/");
    pepper_add_compiler_flag("/Fepepper");
#elif defined(__linux__)
    pepper_set_compiler("g++");
    pepper_add_compiler_flag("src/main.cpp");
    pepper_add_compiler_flag("-I ./include/");
    pepper_add_compiler_flag("-o pepper");
    pepper_add_compiler_flag("-w");
#endif
    pepper_build(true);
    pepper_output_errors();
return 0;
}

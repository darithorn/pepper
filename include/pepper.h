#ifndef __PEPPER_H__
#define __PEPPER_H__
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#if defined(_WIN32) || defined(_WIN64)
#define __windows__
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#elif defined(__linux__)
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#endif

// If you feel inclined to change this I strongly suggest not to
#define DEFAULT_FLAGS_SIZE 0
// You may change this if you desire
#define DEFAULT_FLAGS_CAPACITY 10
#define DEFAULT_COMPILER_FLAGS_CAPACITY 10
#define PEPPER_FAIL 0
#define PEPPER_SUCCESS 1

// TODO(darithorn): possibly move these into pepper_config

static int error_buffer_size = 128;
static int error_buffer_position;
/* The error buffer contains all errors that have been generated
 * Output the error_buffer to `stderr` with <pepper_output_errors>
 */
static char *error_buffer;

typedef void (*pepper_proc)();
struct pepper_task {
    char *name;
    pepper_proc proc;
};

enum pepper_platform {
    PEPPER_PLATFORM_LINUX,
    PEPPER_PLATFORM_WINDOWS,
    PEPPER_PLATFORM_OSX,
};

/* pepper_alias contains aliases for certain flags to make it easier
 * to add support for custom compilers. If the compiler does not have support
 * for a flag specify `null` as its value. If a flag requires a space after it
 * then include the space along with the flag, e.g. "/I ". If the flag requires
 * a more advanced value you can use format strings, e.g. "-D%s=%s".
 * Currently flags with format strings are very easy to break.
 */
struct pepper_alias {
    // the name of the compiler
    char *name;
    // the command to be executed
    char *command;
    // the flag used by the compiler to add an include directory
    char *include_dir_flag;
    // the flag used to change the output
    char *output_flag;
    // the flag used to add a library directory
    char *library_dir_flag;
    // the flag used to link a library
    char *link_flag;
    // the flag to define a preprocessor value
    char *define_flag;
};

static pepper_alias pepper_default_gcc = { "GNU C Compiler", "gcc", "-I", "-o", "-L", "-l", "-D%s=%s" };
static pepper_alias pepper_default_gpp = { "GNU C++ Compiler", "g++", "-I", "-o", "-L", "-l", "-D%s=%s" };
static pepper_alias pepper_default_cl = { "Visual C Compiler", "cl", "/I ", "/Fe", NULL, NULL, "/D%s=%s"};

int pepper_init();
int pepper_quit();

/// This contains all the configuration options that are available
static struct config {
    // runtime information on current platform
    pepper_platform platform;
    // the directory to place build files into
    char *build_dir;
    // the string containing the compiler command
    pepper_alias *compiler_alias;
    char **compiler_flags;
    // how much memory is allocated
    int compiler_flags_capacity;
    // how much memory is actually being used
    int compiler_flags_size;

    /* these flags are for the flags used while executing a command
     * these are here to allow to be able to easily push flags
     */
    char **flags;
    int flags_capacity;
    int flags_size;

    pepper_task *tasks;
    // refers to amount of allocated memory
    int tasks_capacity;
    // refers to actual number of defined tasks
    int tasks_size;

    int registry_capacity;
    int registry_size;
    pepper_alias **alias_registry;

#ifndef PEPPER_MANUAL_INIT
    /* This is the static constructor for pepper_config and will be called automatically
     * before the first pepper related function call. It is meant to initialize pepper automatically.
     * If the user wishes to have control of when pepper is initialized then `#define PEPPER_MANUAL_INIT` and
     * make a call to <pepper_init>.
     */
    config() {
        pepper_init();
    }
    /* This is the static destructor for pepper_config and will be called automatically (assuming the compiler follows the standard)
     * before the end of the program. It is meant to clean up pepper.
     */
    ~config() {
        pepper_quit();
    }
#endif
} pepper_config;

int pepper_register_alias(pepper_alias *);

/* This is the function that initializes pepper to its default state.
 * The user does not have to call this function manually unless `PEPPER_MANUAL_INIT` is defined.
 * @return PEPPER_SUCCESS on successful initialization and PEPPER_FAIL on failure
 */
int pepper_init() {
    // set defaults
#if defined(__windows__)
    pepper_config.platform = PEPPER_PLATFORM_WINDOWS;
#elif defined(__linux__)
    pepper_config.platform = PEPPER_PLATFORM_LINUX;
#elif defined(TARGET_OS_MAC)
    pepper_config.platform = PEPPER_PLATFORM_OSX;
#endif
    pepper_config.tasks_capacity = 2;
    pepper_config.tasks_size = 0;
    pepper_config.flags_capacity = DEFAULT_FLAGS_CAPACITY;
    pepper_config.flags_size = DEFAULT_FLAGS_SIZE;
    pepper_config.compiler_flags_capacity = DEFAULT_COMPILER_FLAGS_CAPACITY;
    pepper_config.compiler_flags_size = 0;
    pepper_config.build_dir = "./";

    pepper_config.compiler_flags = new char *[pepper_config.compiler_flags_capacity];
    pepper_config.flags = new char *[pepper_config.flags_capacity];
    pepper_config.tasks = new pepper_task[pepper_config.tasks_capacity];
#if defined(__windows__)
    // set default compilers
    pepper_config.compiler_alias = &pepper_default_cl;
#elif defined(__linux__)
    pepper_config.compiler_alias = &pepper_default_gpp;
#elif defined(TARGET_OS_MAC)
    pepper_config.compiler_alias = &pepper_default_gpp;
#endif

    pepper_config.registry_capacity = 3;
    pepper_config.registry_size = 0;
    pepper_config.alias_registry = new pepper_alias*[pepper_config.registry_capacity];
    pepper_register_alias(&pepper_default_gcc);
    pepper_register_alias(&pepper_default_gpp);
    pepper_register_alias(&pepper_default_cl);
    return PEPPER_SUCCESS;
}

/* Cleans up pepper and all memory usage
 * @return PEPPER_SUCCESS on success and PEPPER_FAILURE on failure
 */
int pepper_quit() {
    delete pepper_config.compiler_flags;
    delete pepper_config.flags;
    delete pepper_config.tasks;
    return PEPPER_SUCCESS;
}

/// PEPPER_UTIL
/* The less user-friendly of pepper_fstring. Use if you need to pass `va_list` directly.
 * @fmt the format string
 * @args the va_list for variadic arguments
 * @return the newly allocated and formatted string. NULL if any error occurred
 */
char *pepper_vfstring(char *fmt, va_list args) {
    int size = (int)strlen(fmt);
    int calc_size = size + 1; // + 1 to include \0
    int i = 0;

    va_list copy_args;
    va_copy(copy_args, args);

    for(; i < size; i++) {
        char c = fmt[i];
        if(c == '%') {
            i++;
            if(i >= size) break;
            switch(fmt[i]) {
                case 'i': // 2147483647
                case 'd':
                case 'u':
                case 'x': // 0x7FFFFFFF
                case 'X': {
                    // - 2 to take into account the %d %i specifier characters
                    calc_size += 30; // 32 - 2
                    va_arg(args, int);
                    break;
                }
                case 'c': {
                    // - 2 to take into account the %c
                    calc_size -= 1; // 1 - 2
                    va_arg(args, char);
                    break;
                }
                case 's': {
                    char * str = va_arg(args, char *);
                    // char *str = (char *)specs[spec_i];
                    calc_size += (int)strlen(str) - 2;
                    break;
                }
            }
        }
    }

    char *buffer = new char[calc_size]; // this is the buffer being written to
    vsprintf(buffer, fmt, copy_args);

    va_end(copy_args);
    return buffer;
}

/* pepper_fstring is a utility function for sprintf
 * that automatically allocates a new string large enough
 * to fit the formatted string.
 * pepper_fstring does not support the entire format specifier prototype
 * pepper_fstring only supports a small subset of specifiers
 * @fmt the format string
 * @... the values for the format
 * @return The newly allocated and formatted string. NULL if an error occurred.
 */
char *pepper_fstring(char *fmt, ...) {
    // TODO(darithorn): somehow combine pepper_vfstring and pepper_fstring
    va_list args;
    va_start(args, fmt);
    char *result = pepper_vfstring(fmt, args);
    va_end(args);
    return result;
}

/* A simple implementation that returns the extension of the file
 * @file the filename including extension
 * @return the extension or an empty string if no extension
 */
int pepper_file_ext(char *file, char *ext) {
#if defined(__windows__)
    // TODO(darithorn): add support for Windows
    return false;
#elif defined(__linux__)
    ext = strchr(file, '.');
    if(!ext) {
        ext = "";
        return PEPPER_FAIL;
    }
    return PEPPER_SUCCESS;
#endif
}

/* Retrieves the absolute path for the specified file or directory
 * @file the specified file or directory
 * @path the resolved/absolute path. Must be large enough to contain path
 */
void pepper_get_absolute(char *file, char *path) {
#if defined(__linux__)
    realpath(file, path);
#else
    path = "";
#endif
}

/* Checks if the specified directory exists.
 * @directory the directory to check
 * @return whether the directory exists or not
 */
bool pepper_dir_exists(char *directory) {
#if defined(__windows__)
    // TODO(darithorn): add support for Windows
    return false;
#elif defined(__linux__)
    DIR *tmp = opendir(directory);
    if(tmp == NULL) return false;
    closedir(tmp);
    return true;
#else
    return false;
#endif
}

/* Checks if the specified file exists
 * @file the file to check
 * @return whether the file exists or not
 */
bool pepper_file_exists(char *file) {
    // TODO(darithorn): support the wild card operator
#if defined(__windows__)
    // TODO(darithorn): add support for Windows
    return false;
#elif defined(__linux__)
    struct stat s;
    if(stat(file, &s) < 0) return false;
    return true;
#else
    return false;
#endif
}

/* Recursively searches through directories looking for the specified file
 * Be careful as if `start_dir` contains many subdirectories the function may take a while to complete
 * To avoid completely stalling the function halts after 99 directories even if `levels` is -1
 * @start_dir the directory to start the search in
 * @file the file to search for
 * @filename the file including its absolute path if `file` was found
 * @levels the number of subdirectories to search through - specify -1 for unlimited
 * @return whether the file was found or not
 */
bool pepper_search_file(char *start_dir, char *file, char *filename, int levels) {
#if defined(__windows__)
    // TODO(darithorn): add support for Windows
    return false;
#elif defined(__linux__)
    DIR *dir = opendir(start_dir);
    if(dir == NULL) return false;
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL) {
        if(!strcmp(file, entry->d_name)) {
            realpath(entry->d_name, filename);
            return true;
        }
    }
    rewinddir(dir);
    while((entry = readdir(dir)) != NULL) {
        if(entry->d_type == DT_DIR && levels != 0) {
            int l = levels == -1 ? 99 : levels - 1;
            if(pepper_search_file(entry->d_name, file, filename, l)) return true;
        }
    }
#else
    return false;
#endif
}
/// end PEPPER_UTIL

/* Adds an error to the <error_buffer>
 * The error may be formatted and uses <pepper_fstring>
 */
void pepper_add_error(char *msg, ...) {
    if(error_buffer == NULL) {
        error_buffer = new char[sizeof(char) * error_buffer_size];
        // prevents garbage from cluttering the buffer
        error_buffer[0] = '\0';
    }
    int len = (int)strlen(msg) + error_buffer_position;
    if(len >= error_buffer_size) {
        // TODO(darithorn): resize by multiplying by value instead of simply adding length of msg
        // this causes it to realloc every new msg
        error_buffer = (char *)realloc(error_buffer, sizeof(char) * error_buffer_size + len);
        error_buffer_size += len;
    }
    va_list args;
    va_start(args, msg);
    char *tmp = pepper_vfstring(msg, args);
    strcat(error_buffer, tmp);
    strcat(error_buffer, "\n");
    va_end(args);
    delete[] tmp;
}

/* Outputs the <error_buffer> onto `stdout`
 */
void pepper_output_errors() {
    if(error_buffer == NULL) return;
    fprintf(stdout, error_buffer);
}


/* This is used to push flags that are used with the next executed command.
 * They are separate from compiler flags.
 * These flags can change at any time with a <pepper_clear_flags> call.
 * @flag the flag (or flags) to be specified with the next executed command
 */
void pepper_push_flag(char *flag) {
    pepper_config.flags_size++;
    pepper_config.flags[pepper_config.flags_size - 1] = flag;
    // TODO(darithorn): switch to a while so it doesn't risk overflow if flag is too big
    if(pepper_config.flags_size >= pepper_config.flags_capacity) {
        pepper_config.flags_capacity = (int)(pepper_config.flags_capacity * 1.25f);
        pepper_config.flags = (char **)realloc(pepper_config.flags, sizeof(char *) * pepper_config.flags_capacity);
    }
}

/* Clears the set flags
 */
void pepper_clear_flags() {
    // TODO(darithorn): maybe just set flags_size to 0 instead of deleting and then allocating new memory
    pepper_config.flags_capacity = DEFAULT_FLAGS_CAPACITY;
    pepper_config.flags_size = DEFAULT_FLAGS_SIZE;
    delete pepper_config.flags;
    pepper_config.flags = new char *[pepper_config.flags_capacity];
}

/* Executes the given `command` on the command line interface with the specified flags
 * @command the command to execute
 * @flags an array of strings specifying the flags to be passed into the command
 * @flags_n the number of array items
 * @verbose display the command output to the terminal
 * @return PEPPER_SUCCESS on sucess and PEPPER_FAIL and failure
 */
int pepper_exec(char *command, char **flags, int flags_n,  bool verbose) {
    if(command == NULL) {
        pepper_add_error("pepper_exec: command is null\n");
        return PEPPER_FAIL;
    }
    if(flags == NULL) {
        char *tmp[] = { "" };
        flags = tmp;
    }
    int result_i = (int)strlen(command);
    int result_size = 128;
    char *result = new char[result_size];
    strcpy(result, command);
    int i  = 0;
    for(; i < flags_n; i++) {
        int len = (int)strlen(flags[i]) + 2; // + 2 instead of + 1 to include extra space
        if(len + result_i >= result_size) {
            int size = (2 * result_size);
            if(size < len) size += len;
            result_size += size;
            result = (char *)realloc(result, sizeof(char) * result_size);
        }
        result_i += len;
        strcat(result, " ");
        strcat(result, flags[i]);
    }
#if defined(__windows__)
    FILE *tmp = _popen(result, "r");
    if(tmp == NULL) return PEPPER_FAIL;
    if(verbose) {
        char buffer[128];
        while(!feof(tmp)) {
            if(fgets(buffer, 128, tmp) != NULL) {
                fprintf(stdout, buffer);
            }
        }
    }
    fclose(tmp);
#elif defined(__linux__)
    FILE *tmp = popen(result, "r");
    if(tmp == NULL) return PEPPER_FAIL;
    if(verbose) {
        char buffer[128];
        while(!feof(tmp)) {
            if(fgets(buffer, 128, tmp) != NULL) {
                fprintf(stdout, buffer);
            }
        }
    }
    pclose(tmp);
#else
    pepper_add_error("This platform isn't supported\n");
    return PEPPER_FAIL;
#endif
    return PEPPER_SUCCESS;
}

/* Sets the build directory
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure.
 */
int pepper_build_dir(char *dir) {
    // TODO(darithorn): change relative paths to absolute paths
    pepper_config.build_dir = dir;
    return PEPPER_SUCCESS;
}

int pepper_register_alias(pepper_alias *alias) {
    pepper_config.alias_registry[pepper_config.registry_size] = alias;
    pepper_config.registry_size++;
    if(pepper_config.registry_size >= pepper_config.registry_capacity) {
        pepper_config.registry_capacity *= 2;
        pepper_config.alias_registry = (pepper_alias **)realloc(pepper_config.alias_registry, sizeof(pepper_alias *) * pepper_config.registry_capacity);
    }
    return PEPPER_SUCCESS;
}

/* Searches through registered compiler alias _commands_ for the _first_ alias
 * that matches `compiler`. If none is found an error is pushed and PEPPER_FAIL is returned.
 * @compiler the compiler to use
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure.
 */
int pepper_set_compiler(char *compiler) {
    int i = 0;
    for(; i < pepper_config.registry_size; i++) {
        pepper_alias *alias = pepper_config.alias_registry[i];
        if(!strcmp(alias->command, compiler)) {
            pepper_config.compiler_alias = alias;
            return PEPPER_SUCCESS;
        }
    }
    pepper_add_error("%s is not registered as a compiler alias", compiler);
    pepper_config.compiler_alias = NULL;
    return PEPPER_FAIL;
}

/* This adds a flag to be used while compiling.
 * These flags are persistent throughout the configuration.
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure.
 */
int pepper_add_compiler_flag(char *flag, ...) {
    // TODO(darithorn): change relative paths to absolute paths
    va_list args;
    va_start(args, flag);
    pepper_vfstring(flag, args);
    va_end(args);
    pepper_config.compiler_flags[pepper_config.compiler_flags_size] = flag;
    pepper_config.compiler_flags_size++;
    // TODO(darithorn): switch to a while so it doesn't risk overflow if flag is too big
    if(pepper_config.compiler_flags_size >= pepper_config.compiler_flags_capacity) {
        pepper_config.compiler_flags_capacity = (int)(pepper_config.compiler_flags_capacity * 2);
        /*char **tmp = new char *[pepper_config.compiler_flags_capacity];
        int i = 0;
        for(; i < pepper_config.compiler_flags_size; i++) {
            tmp[i] = new char[(int)strlen(pepper_config.compiler_flags[i])];
            strcpy(tmp[i], pepper_config.compiler_flags[i]);
        }
        delete pepper_config.compiler_flags;
        pepper_config.compiler_flags = tmp; */
        pepper_config.compiler_flags = (char **)realloc(pepper_config.compiler_flags, sizeof(char *) * pepper_config.compiler_flags_capacity);
    }
    return PEPPER_SUCCESS;
}

/* Defines a preprocessor value using the proper compiler flag for the currently selected compiler
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure.
 */
int pepper_define_flag(char *name, char *value) {
    // TODO(darithorn): make the use of flags with format strings safer
    return pepper_add_compiler_flag(pepper_config.compiler_alias->define_flag, name, value);
}

enum pepper_vcs {
    PEPPER_VCS_GIT,
    PEPPER_VCS_MERCURIAL,
    PEPPER_VCS_SVN
};

/* Clones a repository specified by url into the dependencies folder (default `libs`)
 * specified in <pepper_config>.
 * @url the repository url
 * @vcs the version control system to use
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure
 */
int pepper_clone_repo(char *url, pepper_vcs vcs) {
    // TODO(darithorn): figure out how to retrieve the files only and not create a local repo
    switch(vcs) {
        case PEPPER_VCS_GIT: {
            char *flags[3] = { "--depth 1", "clone", url };
            return pepper_exec("git", flags, 3, 1);
        }
        case PEPPER_VCS_MERCURIAL: {
            char *flags[2] = { "clone", url };
            return pepper_exec("hg", flags, 2, 1);
        }
        case PEPPER_VCS_SVN: {
            char *flags[2] = { "checkout", url };
            return pepper_exec("svn", flags, 2, 1);
        }
        default:
            pepper_add_error("Unsupported version control system");
            return PEPPER_FAIL;
    }
}

/* Adds a task that will be ran before pepper builds the configuration
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure.
 */
int pepper_create_task(char *name, pepper_proc proc) {
    pepper_config.tasks_size++;
    pepper_task task = {name, proc};
    pepper_config.tasks[pepper_config.tasks_size - 1] = task;
    // TODO(darithorn): switch to a while so it doesn't risk overflow if flag is too big
    if(pepper_config.tasks_size >= pepper_config.tasks_capacity) {
        pepper_config.tasks_capacity = pepper_config.tasks_capacity * 2;
        pepper_config.tasks = (pepper_task *)realloc(pepper_config.tasks, sizeof(pepper_task) * pepper_config.tasks_capacity);
    }
    return PEPPER_SUCCESS;
}

/* Runs the tasks and executes the selected compiler with the set compiler flags
 * @return PEPPER_SUCCESS on success and PEPPER_FAIL on failure.
 */
int pepper_build(bool verbose) {
    if(pepper_config.compiler_alias == NULL) {
        pepper_add_error("No compiler alias set to be used");
        return PEPPER_FAIL;
    }
    int i = 0;
    for(; i < pepper_config.tasks_size; i++) {
        pepper_task task = pepper_config.tasks[i];
        fprintf(stdout, "Executing task [%i of %i]: %s\n", i + 1, pepper_config.tasks_size, task.name);
        task.proc();
    }
    pepper_exec(pepper_config.compiler_alias->command, pepper_config.compiler_flags, pepper_config.compiler_flags_size, verbose);
    return PEPPER_SUCCESS;
}

#endif // __PEPPER_H__
